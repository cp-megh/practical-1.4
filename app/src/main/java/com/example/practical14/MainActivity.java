package com.example.practical14;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.practical14.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int READ_STORAGE_CODE = 101;
    private static final int WRITE_STORAGE_CODE = 102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.btnWriteRequest.setOnClickListener(view1 -> {
            checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_STORAGE_CODE);
        });

        binding.btnReadRequest.setOnClickListener(view1 -> {
            checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_STORAGE_CODE);
        });

        binding.btnCameraRequest.setOnClickListener(view1 -> {
            checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
        });
    }

    private void checkPermission(String permission, int requestCode) {

        if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED) {
            //Request for permission
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        } else {
            Toast.makeText(this, R.string.permission_granted, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.camera_permission_granted, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.camera_permission_denied, Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == READ_STORAGE_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.read_permission_granted, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.read_permission_denied, Toast.LENGTH_SHORT).show();

            }
        }

        if (requestCode == WRITE_STORAGE_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.write_permission_granted, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.write_permission_denied, Toast.LENGTH_SHORT).show();

            }
        }

    }
}